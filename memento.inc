<?php

function _memento_load_patches() {
  global $memento_patches, $user;
  
  $memento_patches = array();
  
  $result = db_select('memento_patches', 'cp')
    ->fields('cp')
    ->condition('uid', $user->uid)
    ->execute();

  while($record = $result->fetchAssoc()) {
    $patch = unserialize($record['patch']);
    $patch['status'] = $record['status'];
    $patch['id'] = $record['id'];
    $memento_patches[] = $patch;
  }
}

function _memento_initialize_language() {
  $types = language_types();

  // Ensure the language is correctly returned, even without multilanguage
  // support. Also make sure we have a $language fallback, in case a language
  // negotiation callback needs to do a full bootstrap.
  // Useful for eg. XML/HTML 'lang' attributes.
  $default = language_default();
  foreach ($types as $type) {
    $GLOBALS[$type] = $default;
  }
}

function _memento_start_transaction() {
  global $memento_transaction, $memento_patches;
  if (!isset($memento_transaction)) {
    register_shutdown_function('_memento_shutdown');
    $memento_transaction = db_transaction();
  }
}

function _memento_apply_active_patches() {
  global $memento_transaction, $memento_patches;
  
  // Double check if the memento transaction is open.
  if (isset($memento_transaction)) {
    module_load_include('inc', 'memento', 'memento.node');
    foreach ($memento_patches as $patch) {
      if ($patch['status']) {
        memento_node_save($patch['old'], $patch['new'], NULL);      
      }
    }
  }
}

function _memento_shutdown()
{
  global $memento_transaction, $content_patch, $user;
  
  // Rollback all changes made up to this point.
  if (isset($memento_transaction)) {
    $memento_transaction->rollback();
    if (isset($content_patch)) {
      // Store patch into database.
      $serialized_patch = serialize($content_patch);
      unset($content_patch);
      
      $nid = db_insert('memento_patches')
        ->fields(array(
          'uid' => $user->uid,
          'status' => TRUE,
          'name' => 'patch',
          'patch' => $serialized_patch,
      ))->execute();
    }
  }
}

function _memento_activate_patch($id, $status) {
  global $user;
  
  db_update('memento_patches')
    ->fields(array('status' => $status))
    ->condition('id', $id)
    ->condition('uid', $user->uid)
    ->execute();
  
  drupal_goto('<front>');
}

function _memento_record() {
  $recording = _memento_is_recording();
  
  // Switch the recording flag.
  $_SESSION['memento_recording'] = !$recording;
  
  drupal_goto('<front>');
}

function memento_output() {

  $links = array(
    '#theme' => 'memento_links',
    '#weight' => -100,
  );
  
  // @todo Always output container to harden JS-less support.
  $links['#prefix'] = '<div id="memento"><div id="memento-wrapper">';
  $links['#suffix'] = '</div></div>';
  
  $links['hide'] = array(
    '#title' => t('Hide'),
    '#attributes' => array('class' => array('memento-item')),
    '#href' => '<front>',
    '#options' => array(
      'html' => TRUE,
    ),
  );
  
  global $memento_patches;  
  if (isset($memento_patches) && !empty($memento_patches)) {
    $links['patches'] = array(
      '#title' => t('Patches'),
      '#attributes' => array('class' => array('memento-item')),
      '#href' => '<front>',
      '#options' => array(
        'html' => TRUE,
      ),
    );
  
    foreach ($memento_patches as $patch) {
      $id = $patch['id'];
      $title = $patch['new']->title;
      $status = $patch['status'];
      
      $classes = array('memento-item');
      
      $inactive = '';
      if ($status == '0') {
        $classes[] = 'memento-inactive';
        $inactive = ' (inactive)';
      }
      
      $links['patches'][$id] = array(
        '#title' => $title . $inactive,
        '#attributes' => array('class' => $classes),
        '#href' => 'memento/apply/' . $id . '/' . ($status ? '0' : '1'),
      );
    }
  }
  
  $class = _memento_is_recording() ? 'memento-recording-on' : 'memento-recording-off';
  
  $links['record'] = array(
    '#title' => t('Record & Preview'),
    '#attributes' => array('class' => array($class)),
    '#href' => 'memento/record',
    '#options' => array(
      'html' => TRUE,
    ),
  );
  
  return $links;
}
